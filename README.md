## CCRPC Land Use Model

### [Parcel level land use model based on UrbanSim](#parcel-level)

UrbanSim is a urban simulation framework that empowers users to run land use modeling and evaluate urban development plans.

The most detailed and powerful of the model applications of UrbanSim use individual buildings and parcels to represent location. Most of the current UrbanSim users among Metropolitan Planning Organizations in the United States use UrbanSim at this level.

### [Features](#features)

- **Household Models**

From one year to the next, households choose whether to move or stay in their current residence, and if they choose to move, they make a location choice from the available (vacant) housing units. Factors such as housing prices and rents, accessibility, and local neighborhood composition all influence location choices of households of different incomes, sizes, and stages of life cycle.

- **Employment Models**

From one year to the next, jobs choose whether to move or stay in their current location, and if they choose to move, they make a location choice from the available buildings and parcels that have remaining capacity for jobs. Factors such as accessibility to jobs and population, and local employment composition all influence location choices of jobs of different industry sectors.

- **Real Estate Models**

From one year to the next, buildings (and the residential units or job spaces inside buildings) are constructed and added to parcels using a pro forma-based real estate development model and price models. Vacancy rates in each submarket (residential building type - tenure combination), and the location of these units is based on factors such as accessibility, prices, rents, and other local factors, subject to development constraints imposed by local jurisdictions.

Source: https://urbansim.com/urbansim-parcel-model

### [Installation Guide](#installation-guide)

1. On your local machine, clone this repo

```bash
# Clone with SSH
git clone git@gitlab.com:ccrpc/land-use-model.git

# Or clone with HTTPS
git clone https://gitlab.com/ccrpc/land-use-model.git
```

2. Build the docker image. If you haven't got Docker, please [follow these installation instructions](https://docs.docker.com/install/)

```bash
cd land-use-model
docker build -t land-use-model -t ccrpc_urbansim:latest .
```

3. Start a single container

```bash
docker run -p --rm 8888:8888 ccrpc_urbansim:latest
```

### [Developer Guide](#developer-guide)

See [data preparation overview](https://gitlab.com/ccrpc/land-use-model/-/wikis/home) to learn more about data preparation, and check out [CCRPC Synthpop repository](https://gitlab.com/ccrpc/ccrpc-synthopop) for how we generagte synthetic population.

Once the data is ready, we can start the simulation workflow. This implementation is based on the [complete example - San Francisco UrbanSim Modules](https://udst.github.io/urbansim/examples.html). To understand the workflow, it is important to how the following four python scripts fit together. The workflow is shown in the illustration below.

![UrbanSim pipeline (2)](static/UrbanSim_workflow.png)

Next we will discuss some essential scripts and modules of the UrbanSim model.

#### [dataset.py](#dataset.py)

This module loads/registers data into the UrbanSim Framework. The base year input data is stored as a `pd.HDFStore` instance. Tables are registered with the [@orca.table](https://udst.github.io/orca/core.html#tables) decorator. For instance, the following code will register the `household_controls` table.

```python
@orca.table("household_controls", cache=True)
def household_controls(store):
    df = store["household_controls"]
    df[df < 0] = np.inf
    return df
```

Note that Orca supports data wrangling in Python or/and Pandas when we register the table. See `zoning_baseline` table as an example.

This implementation includes mappings of tables stored in the HDF5 file to table sources for a typical UrbanSim schema, which consists of `parcels`, `buildings`, `households`, `jobs`, `zoning` and `zones`. Please refer to data preparation section for required columns and data types in each table.

The last step of table registration is to use [orca.broadcast function](https://udst.github.io/orca/core.html#orca.orca.broadcast) to register the relationships between all tables. The example below joins `zones` table and `buildings` table based on `zone_id`:

```python
orca.broadcast("zones", "buildings", cast_index=True, onto_on="zone_id")
```

#### [assumptions.py](#assumption.py)

This is the module where we make "assumptions" about the model. In fact it may be helpful not to think of these steps as making assumption, they are simply configuring the model in another way.

Most importantly, we need to specify the `store` to be used in the simulation. This is where the data is from.

Next we need to do some mappings. It is typically achieved by calling Orca's [add_injectable](https://udst.github.io/orca/core.html#orca.orca.add_injectable) API. In the following example, we use a Python dictionary to map integer of building type to string of land use category names. Subsequently we make an assumption about the number of jobs per square feet for each type of building.

```python
orca.add_injectable(
    "building_type_map",
    {
        1: "Residential",
        2: "Residential",
        3: "Retail",
        4: "Industrial",
        5: "Residential",
        6: "Office",
        7: "Residential",
        8: "Industrial",
        9: "Industrial",
        10: "Office",
        11: "Office",
        12: "Office",
        13: "Industrial",
    },
)

orca.add_injectable(
    "building_sqft_per_job",
    {
        -1: 400,
        1: 400,
        2: 400,
        3: 400,
        4: 355,
        5: 1161,
        6: 470,
        7: 661,
        8: 960,
        9: 825,
        10: 445,
        11: 445,
        12: 383,
        13: 383,
    },
)
```

#### [variables.py](#variables.py)

`variables.py` is the module where we dynamically compute columns from existing or other computed columns. To add a computed column, we use Orca decorator [@orca.column](https://udst.github.io/orca/core.html#columns)

By convention the function name is the same as the name of the computed column. A typical example is shown below.

```python
@orca.column("parcels", "total_buildings", cache=True, cache_scope="iteration")
def total_buildings(parcels, buildings):
    return (
        buildings.building_id.groupby(buildings.parcel_id)
        .count()
        .reindex(parcels.index)
        .fillna(0)
    )
```

This creates a new column `total_buildings` for the `parcels` table. The computed colum can also be used in other computed columns thanks to dependency injection of the Orca framework. Note that although we can use `DataFrame` method on the `buildings` object, it is in fact a [DataFrameWrapper](https://udst.github.io/orca/core.html#orca.orca.DataFrameWrapper) object, therefore, not ll Pandas DataFrame methods are supported. In order to convert `DataFrameWrapper` to a `DataFrame`, we can call [to_frame](https://udst.github.io/orca/core.html#orca.orca.DataFrameWrapper.to_frame) method, which returns all computed columns on the table and so has performance implications. See [a concrete example](https://udst.github.io/urbansim/examples.html#variables).

#### [models.py](#models.py)

The purpose of models.py is to define custom estimations or simulations that usually utilize the lower-level APIs(regression model, location choice model, etc, )) from the UrbanSim library.

Most of the models use configuration files to define the actual model configuration. The user will specify the model configurations in a yaml file. The estimation function will read the configuration and write the estimated coefficients back to the same configuration file or a separate output file that can be specified.

For instance, the function below decorated by [@orca.step](https://udst.github.io/orca/core.html#steps) will run the hedonic estimation for non-residential rent. It reads configuration from `nrh.yaml` file and append the coefficients of fit parameters to the same file.

```python
@orca.step("nrh_estimate")
def nrh_estimate(buildings, zones):
    return utils.hedonic_estimate("nrh.yaml", buildings, zones)
```

For simulations, the model takes the estimation results we generated and runs simulation. The simulation results, which are usually columns(Panda Series), will be saved to the existing tables that are in the form of [DataFrameWrapper](https://udst.github.io/orca/core.html#orca.orca.DataFrameWrapper).

```python
@orca.step("hlcm_simulate")
def hlcm_simulate(households, buildings, zones):
    return utils.lcm_simulate(
        "hlcmcoef.yaml",
        households,
        buildings,
        zones,
        "building_id",
        "residential_units",
        "vacant_residential_units",
    )
```

In this implementation, we adopted a simple `relocation` model from the [San Francisco full example](https://github.com/udst/sanfran_urbansim). In terms of `transition`, we added `control totals` to number of households and number of jobs.

Users can create custom simulations. The custom function doesn't not have to read configurations from yaml file and it can directly update data in UrbanSim tables. See group quarter scaling function `gq_scaling` as an example.

#### [Model Configuration](#module-configuration)

```python
name: rsh

model_type: regression

fit_filters:

    - unit_lot_size > 0
    - year_built > 1000
    - year_built < 2020
    - unit_sqft > 100
    - unit_sqft < 20000

predict_filters:

    - general_type == 'Residential'

model_expression: np.log1p(residential_sales_price) ~ I(year_built < 1940) + I(year_built
    > 2005) + np.log1p(unit_sqft) + np.log1p(unit_lot_size) + sum_residential_units

    - ave_lot_sqft + ave_unit_sqft + ave_income

ytransform: np.exp
```

#### [simulation.py](#simulation.py)

The entry point of the project is [Simulation.py](simulation.py).

In the simulation workflow, the transition and relocation model are placed before the Location Choice Model(LCM). The sequence is a convention we established internally. We are assuming that our data(households, jobs, etc) refers to the number at the beginning of the year(including base year and control tables). In reality, those decisions are often being made throughout the course of the year.

In general, there are two types of steps. One is _estimation_ and the other one is _simulation_. The _estimation_ step builds the model based on some configurations and then the _simulation_ makes predictions based on the statistical model. UrbanSim framework provides APIs that facilitate both steps.

For more detailed information about how to configure the model and how to call the APIs, please refer to the [UrbanSim documentation](https://udst.github.io/urbansim/models/statistical.html#api).

Next we will illustrate how to build the model(_estimation_) and run the simulation(_simulation_) through two examples.

#### Example 1 - non-residential rent hedonic estimation

The APIs for _estimation_ in UrbanSim v3.1.1 are shown as below. These APIs invoke functions from the [statsmodels](https://www.statsmodels.org/stable/index.html) module under the hood.

- Regression API

  - **RegressionModel**: A hedonic (regression) model with the ability to store an estimated model and predict new data based on the model.

  - **SegmentationRegressionModel**: A regression model group that allows segments to have different model expressions and ytransforms but all have the same filters.

  - **RegressionModelGroup**: A regression model group that allows segments to have different model expressions and ytransforms but all have the same filters.

* Discrete Choice API

  - **MNLDiscreteChoiceModel**: A discrete choice model with the ability to store an estimated model and predict new data based on the model.

  - **SegmentedMNLDiscreteChoiceModel**: An MNL LCM group that allows segments to have different model expressions but otherwise share configurations.

  - **MNLDiscreteChoiceGroup**: Manages a group of discrete choice models that refer to different segments of choosers.

We use non-residential rent hedonic estimation as an example.

```python
@orca.step("nrh_estimate")
def nrh_estimate(buildings, zones):
    return utils.hedonic_estimate("nrh.yaml", buildings, zones)
```

As described in [Model Configuration](#module-configuration), the configuration parameters for this estimation is saved in the `nrh.yaml` file. We specify the `model_type` to be `segmented_regression`, also known as `piecewise regression`. The basic idea is that non-residential rent data follows different trends over different `general_type` of buildings.

```yaml
model_type: segmented_regression
segmentation_col: general_type
```

Here we created four categories: `Residential`, `Retail`, `Office`, and `Industrial`, obviously we need to exclude `Residential` type in this estimation. We will also skip segments with less than 10 samples as they may skew the model.

```yaml
fit_filters:
  - general_type in ['Retail', 'Office', 'Industrial']
predict_filters:
  - general_type != 'Residential'
```

We describe our model with `default_config` using is a [pasty](https://patsy.readthedocs.io/en/latest/index.html) expression.

```yaml
model_expression: np.log1p(non_residential_rent) ~ I(year_built < 1940) + I(year_built
        > 2005) + np.log1p(stories) + ave_income + jobs
    ytransform: np.exp
```

The predicted output is the log of the rent, therefore we add `ytransform: np.exp` to perform the inverse operation to get the actual rent price.

By default, the statistical model will be saved on the same yaml file unless specified otherwise. The simulation step will read the fit parameters and their coefficients from this file.

The estimation step invokes the `hedonic_estimate` function in the `utils` module. This function will read the configuration file and instantiate the corresponding UrbanSim model class. In the above example, two input tables are pass into the `hedonic_estimate` function, `buildings` and `zones`. These two (or any number of) tables will be merged by the `to_frame` function(invoked by `hedonic_estimate`) of the `utils` module. Tables must have registered merge rules via the broadcast function, which we did in [dataset.py](#dataset.py). `NaN` in the merged dataframe will be then dealt with according to the rules we specified. Last, `hedonic_estimate` will call classmethod `fit_from_cfg` to generate the model. Note that the goodness of fit report will be printed out to the console. It is important to interpret and evaluate the results before using the model to run simulations. This is why there is a separate `estimation.py` file. In the first phase of this project, we were only able to create an extremely simplified model and we have very limited data available, thefore our model fails to adequately describe the functional relationship between the independant variable(rent) and experimental factors, we would like to correct the lack of fit in the next phase.

#### Example 2 - job relocation


The API for relocation model is:

- **RelocationModel**: Find movers within a population according to a table of relocation rates.


We are only applying a simple relocation model. In this step, the model chooses movers based on prescribed relocation rates.. 

```python
@orca.step("jobs_relocation")
def jobs_relocation(jobs):
    return utils.simple_relocation(jobs, 0.01, "building_id")
```

The signature of `simple_relocation` function is

```python
def simple_relocation(choosers, relocation_rate, fieldname)
```

Setting the relocation rate to 0.01 will randomly select 1% choosers(jobs) without replacement and mark the fieldname(building_id) of selected choosers with `-1`. The `-1` is the flag we will rely on to target movers in the location choice model simulation.


#### Example 3 - jobs transition

The APIs for transition model are:

- **GrowthRateTransition**: Transition given tables using a simple growth rate.
- **TabularGrowthTransition**: Growth rate based transitions where the rates are stored in a table indexed by year with optional segmentation.
- **TabularTotalsTransition**: Transition data via control totals in pandas DataFrame with optional segmentation.
- **TransitionModel**: Models things moving into or out of a region.
  
In the third example, we will illustrate job transition simulation with control totals.

```python
@orca.step("jobs_transition")
def jobs_transition(jobs, employment_controls, year):
    orig_size = jobs.local.shape[0]
    res = utils.full_transition(
        jobs,
        employment_controls,
        year,
        {"total_column": "total_number_of_jobs"},
        "building_id",
    )
    print(
        "Net change: %s jobs"
        % (orca.get_table("jobs").local.shape[0] - orig_size)
    )
    return res
```

The control setting `{"total_column": "total_number_of_jobs"}` is stored in a Python dictionary, we could have saved it in a ymal file and added more custom options. The implemtation of `full_transition` is quite self-explanatory, please refer to utils.py. Note the `building_id` column of newly added jobs will be temporarily assigned to `-1`, which is similar to what we have done in the relocation step. 


#### Example 4 - employment location choice model simulation

The employment location choice model is a Segmented Multinomial Logit(MNL) Location Choice Model. To understand how prediction works, please refer to the source code of classmethod [predict_from_cfg](https://udst.github.io/urbansim/_modules/urbansim/models/dcm.html#SegmentedMNLDiscreteChoiceModel.predict_from_cfg).

Remember we registered auxiliary columns for the buildings table, e.g., `sqft_per_job`, `job_spaces`, `vacant_residential_units` in the variables.py file, as shown below.

```python
""" variables.py """

@orca.column("buildings", "sqft_per_job", cache=True, cache_scope="iteration")
def sqft_per_job(buildings, building_sqft_per_job):
    return buildings.building_type_id.fillna(-1).map(building_sqft_per_job)

@orca.column("buildings", "job_spaces", cache=True, cache_scope="iteration")
def job_spaces(buildings):
    return (
        (buildings.non_residential_sqft / buildings.sqft_per_job)
        .fillna(0)
        .astype("int")
    )

# ...

@orca.column("buildings", "vacant_residential_units")
def vacant_residential_units(buildings, households):
    return buildings.residential_units.sub(
        households.building_id.value_counts(), fill_value=0
    )
```

The result of the prediction will be reflected in the choosers(jobs) table. Also columns in the buildings table will be updated to the status at the end of the location choice simulation cycle, will means some vacant buildings are now filled with jobs and the `building_id` of those jobs are now linked to buildings. 


#### Comment


At the time of developing this project, UrbanSim has been working on creating more targeted packages to replace some of the functionalities in the current library:

> - task orchestration is now handled by Orca
> - real estate development logic is now in Developer
> - discrete choice logic is now in ChoiceModels
> - reusable model step patterns are in UrbanSim Templates

Not all above packages have stable release yet. The stable releases will most likely be made available along with UrbanSim v3.2 as discussed [here](https://github.com/UDST/urbansim/issues/218), the current UrbanSim version is 3.1.1. We will revisit and refactor this repository when the new release becomes available.

### [Visualization](#visualization)
The simulation consists of the following steps:

- **rsh_estimate** -> residential sales hedonic estimation
- **nrh_estimate** -> non-residential rent hedonic estimation
- **rsh_simulate** -> residential sales hedonic
- **nrh_simulate** -> non-residential rent hedonic
- **household_relocation** -> households relocation model
- **household_transition** -> households transition
- **gq_scaling** -> group quarter population scaling
- **hlcm_simulate** -> households location choice model
- **jobs_relocation** -> jobs relocation model
- **jobs_transition** -> jobs transition
- **elcm_simulate** -> employment location choice model
- **feasibility** -> compute development feasibility
- **residential_developer** -> develop residential buildings
- **non_residential_developer** -> develop non-residential buildings

We deployed a web map application to [RPC's website](https://maps.ccrpc.org/land-use/) with sample data and simulation results from 2015-2045. The application was developed based on [UrbanSim's open source maps](https://github.com/UDST/urbansim/tree/master/urbansim/maps).
![Webmap Screenshot](static/webmap_screenshot.png)

### [Contribute](#contribute)

- Issue Tracker: https://gitlab.com/ccrpc/land-use-model/issues
- Source Code: https://gitlab.com/ccrpc/land-use-model

### [Support](#support)

If you are having issues, please let us know by [opening an issue](https://gitlab.com/ccrpc/land-use-model/issues).

### [Credit](#credit)

This application uses Open Source components. You can find the source code of their open source projects along with license information below. We acknowledge and are grateful to these developers for their contributions to open source.

Project: Synthpop [https://github.com/UDST/synthpop](https://github.com/UDST/synthpop)
Copyright 2016, UrbanSim Inc. All right reserved.
License (BSD 3-Clause "New" or "Revised")

Project: UrbanSim [https://github.com/UDST/urbansim](https://github.com/UDST/urbansim)
Copyright 2016, UrbanSim Inc. All right reserved.
License (BSD 3-Clause "New" or "Revised")

### [License](#license)

CCRPC Land Use Model is available under the terms of the
[BSD 3-clause license][2].

[1]: https://ccrpc.org/
[2]: https://gitlab.com/ccrpc/land-use-model/blob/master/LICENSE.md
