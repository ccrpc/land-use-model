# Use miniconda3-alpine as a parent image
FROM continuumio/miniconda3:4.7.12-alpine

# Copy the current directory contents into the container at /app
COPY --chown=anaconda:anaconda . /app

# Set the working directory to /app
WORKDIR /app
# Update pip, install Jupyter Notebook and UrbanSim
RUN /opt/conda/bin/pip install --no-cache-dir -r requirements.txt

# Expose port 8888 for Jupyter notebook
EXPOSE 8888

# Open notebook when the container starts
ENTRYPOINT ["/opt/conda/bin/jupyter", "notebook","--ip=0.0.0.0","--allow-root"]
