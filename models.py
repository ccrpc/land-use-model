import random

import orca

# from urbansim_defaults import utils as uutils

import dataset
import utils
import variables


@orca.injectable()
def year(iter_var):
    return iter_var


@orca.step("rsh_estimate")
def rsh_estimate(buildings, zones):
    return utils.hedonic_estimate("rsh.yaml", buildings, zones)


@orca.step("rsh_simulate")
def rsh_simulate(buildings, zones):
    return utils.hedonic_simulate(
        "rsh.yaml", buildings, zones, "residential_sales_price"
    )


@orca.step("nrh_estimate")
def nrh_estimate(buildings, zones):
    return utils.hedonic_estimate("nrh.yaml", buildings, zones)


@orca.step("nrh_simulate")
def nrh_simulate(buildings, zones):
    return utils.hedonic_simulate(
        "nrh.yaml", buildings, zones, "non_residential_rent"
    )


@orca.step("hlcm_estimate")
def hlcm_estimate(households, buildings, parcels, zones):
    return utils.lcm_estimate(
        "hlcm.yaml",
        households,
        "building_id",
        buildings,
        zones,
        out_cfg="./configs/hlcmcoef.yaml",
    )


@orca.step("hlcm_simulate")
def hlcm_simulate(households, buildings, zones):
    return utils.lcm_simulate(
        "hlcmcoef.yaml",
        households,
        buildings,
        zones,
        "building_id",
        "residential_units",
        "vacant_residential_units",
    )


@orca.step("elcm_estimate")
def elcm_estimate(jobs, buildings, zones):
    return utils.lcm_estimate(
        "elcm.yaml",
        jobs,
        "building_id",
        buildings,
        zones,
        out_cfg="./configs/elcmcoef.yaml",
    )


@orca.step("elcm_simulate")
def elcm_simulate(jobs, buildings, zones):
    return utils.lcm_simulate(
        "elcmcoef.yaml",
        jobs,
        buildings,
        zones,
        "building_id",
        "job_spaces",
        "vacant_job_spaces",
    )


@orca.step("households_relocation")
def households_relocation(households):
    return utils.simple_relocation(households, 0, "building_id")


@orca.step("jobs_relocation")
def jobs_relocation(jobs):
    return utils.simple_relocation(jobs, 0.01, "building_id")


@orca.step("households_transition")
def households_transition(households, household_controls, year):
    orig_size_hh = households.local.shape[0]
    res = utils.full_transition(
        households,
        household_controls,
        year,
        {"total_column": "total_number_of_households"},
        "building_id",
    )

    print(
        "Net change: %s households"
        % (orca.get_table("households").local.shape[0] - orig_size_hh)
    )

    # changes to households/persons table are not reflected in local scope
    # need to reset vars to get changes.
    households = orca.get_table("households")
    orca.clear_cache()
    return res


@orca.step("jobs_transition")
def jobs_transition(jobs, employment_controls, year):
    orig_size = jobs.local.shape[0]
    res = utils.full_transition(
        jobs,
        employment_controls,
        year,
        {"total_column": "total_number_of_jobs"},
        "building_id",
    )
    print(
        "Net change: %s jobs"
        % (orca.get_table("jobs").local.shape[0] - orig_size)
    )
    return res


@orca.step("feasibility")
def feasibility(parcels):
    utils.run_feasibility(
        parcels,
        variables.parcel_average_price,
        variables.parcel_is_allowed,
        residential_to_yearly=True,
    )


def random_type(form):
    form_to_btype = orca.get_injectable("form_to_btype")
    return random.choice(form_to_btype[form])


def add_extra_columns(df):
    for col in ["residential_sales_price", "non_residential_rent"]:
        df[col] = 0
    return df


@orca.step("residential_developer")
def residential_developer(feasibility, households, buildings, parcels, year):
    utils.run_developer(
        ["residential", "mixedresidential"],
        households,
        buildings,
        "residential_units",
        parcels.parcel_size,
        parcels.ave_unit_size,
        parcels.total_units,
        feasibility,
        year=year,
        target_vacancy=0.15,
        form_to_btype_callback=random_type,
        add_more_columns_callback=add_extra_columns,
        bldg_sqft_per_job=400.0,
    )


@orca.step("non_residential_developer")
def non_residential_developer(feasibility, jobs, buildings, parcels, year):
    utils.run_developer(
        ["office", "retail", "industrial", "mixedresidential", "mixedoffice"],
        jobs,
        buildings,
        "job_spaces",
        parcels.parcel_size,
        parcels.ave_unit_size,
        parcels.total_job_spaces,
        feasibility,
        year=year,
        target_vacancy=0.15,
        form_to_btype_callback=random_type,
        add_more_columns_callback=add_extra_columns,
        residential=False,
        bldg_sqft_per_job=400.0,
    )


@orca.step("gq_scaling")
def gq_scaling(group_quarters, group_quarters_control_totals, year):
    target_gq = group_quarters_control_totals.to_frame()
    target_pop = target_gq[target_gq.index == year]["population"].sum()

    current_pop = group_quarters.to_frame(group_quarters.local_columns)[
        "population"
    ].sum()
    gqpop = group_quarters.to_frame(group_quarters.local_columns)

    diff = target_pop - current_pop
    if diff > 0:
        selected_gq = gqpop.sample(int(diff), replace=True)
        selected_gq["diff"] = selected_gq.groupby("gq_id")[
            "population"
        ].count()
        selected_gq.drop_duplicates(inplace=True)
        gqpop.loc[
            gqpop.index.isin(selected_gq.index), "population"
        ] += selected_gq["diff"]

    if diff < 0:
        selected_gq = gqpop.sample(int(-diff), replace=True)
        selected_gq["diff"] = selected_gq.groupby("gq_id")[
            "population"
        ].count()
        selected_gq.drop_duplicates(inplace=True)
        gqpop.loc[
            gqpop.index.isin(selected_gq.index), "population"
        ] -= selected_gq["diff"]

    orca.add_table("group_quarters", gqpop[group_quarters.local_columns])
