import orca
import pandas as pd

import models

pd.options.mode.chained_assignment = None

# Hedonic Estimation

orca.run(["rsh_estimate"])
orca.run(["nrh_estimate"])

# Hedonic Simulation
orca.run(["rsh_simulate", "nrh_simulate"])

# LCM Estimation
orca.run(["hlcm_estimate"])
orca.run(["elcm_estimate"])
