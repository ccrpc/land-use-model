import os

import orca
import pandas as pd
from urbansim.utils import misc

orca.add_injectable(
    "building_sqft_per_job",
    {
        -1: 400,
        1: 400,
        2: 400,
        3: 400,
        4: 355,
        5: 1161,
        6: 470,
        7: 661,
        8: 960,
        9: 825,
        10: 445,
        11: 445,
        12: 383,
        13: 383,
    },
)


# this maps building type ids to general building types
# basically just reduces dimensionality
orca.add_injectable(
    "building_type_map",
    {
        1: "Residential",
        2: "Residential",
        3: "Retail",
        4: "Industrial",
        5: "Residential",
        6: "Office",
        7: "Residential",
        8: "Industrial",
        9: "Industrial",
        10: "Office",
        11: "Office",
        12: "Office",
        13: "Industrial",
    },
)


# this maps building "forms" from the developer model
# to building types so that when the developer builds a
# "form" this can be converted for storing as a type
# in the building table - in the long run, the developer
# forms and the building types should be the same and the
# developer model should account for the differences
orca.add_injectable(
    "form_to_btype",
    {
        "residential": [1, 2, 7],
        "industrial": [4, 8, 9, 13],
        "retail": [3],
        "office": [6, 11, 12],
        "mixedresidential": [5],
        "mixedoffice": [10],
    },
)


orca.add_injectable(
    "store",
    pd.HDFStore(os.path.join(misc.data_dir(), "champaign.h5"), mode="r"),
)

orca.add_injectable(
    "fillna_config",
    {
        "buildings": {
            "residential_sales_price": ("zero", "int"),
            "non_residential_rent": ("zero", "int"),
            "residential_units": ("zero", "int"),
            "non_residential_sqft": ("zero", "int"),
            "year_built": ("median", "int"),
            "building_type_id": ("mode", "int"),
        },
        "jobs": {"job_category": ("mode", "str"),},
        # "zones": {"jobs": ("zero", "int")},
    },
)


# this keeps track of all of the inputs that get "switched"
# whenever a scenario is changed
orca.add_injectable(
    "scenario_inputs",
    {
        "baseline": {"zoning_table_name": "zoning_baseline"},
        "test": {"zoning_table_name": "zoning_test"},
    },
)


orca.add_injectable("scenario", "baseline")
