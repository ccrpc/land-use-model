import os
import argparse
import time
from pathlib import Path

import orca
import pandas as pd
from urbansim.utils import misc, yamlio

import models
import utils

import argparse

parser = argparse.ArgumentParser(
    description="Configure simulation start and end year. [start, end)"
)
parser.add_argument(
    "--start",
    default=2015,
    help="start year[INCLUSIVE]  (default: 2015)",
    type=int,
)
parser.add_argument(
    "--end",
    default=2017,
    help="end year[EXCLUSIVE] (default: 2017)",
    type=int,
)
args = parser.parse_args()


input_csv_dir = f"Input"

input_dir = "data"
input_hdf_filename = "champaign.h5"
input_hdf = Path.cwd().joinpath(input_dir).joinpath(input_hdf_filename)

try:
    print(input_hdf)
    os.remove(input_hdf)
except OSError:
    pass


tables = {
    "buildings": "building_id",
    "households": "household_id",
    "jobs": "job_id",
    "parcels": "parcel_id",
    "zones": "zone_id",
    "zoning_for_parcels": None,
    "zoning": "id",
    "group_quarters": "gq_id",
    "household_controls": "year",
    "employment_controls": "year",
    "group_quarters_control_totals": "year",
}


with pd.HDFStore(input_hdf) as store:
    for table, tid in tables.items():
        f = os.path.join(input_csv_dir, table + ".csv")
        store[table] = pd.read_csv(f, index_col=tid)


cwd = os.getcwd()
timestr = time.strftime("%Y%m%d-%H%M%S")
data_out = os.path.join(cwd, "Simulations", f"result_{timestr}.h5")

orca.run(
    [
        "rsh_estimate",  # residential sales hedonic estimation
        "nrh_estimate",  # non-residential rent hedonic estimation
        "rsh_simulate",  # residential sales hedonic
        "nrh_simulate",  # non-residential rent hedonic
        "households_relocation",  # households relocation model
        "households_transition",  # households transition
        "gq_scaling",  # group quarter population scaling
        "hlcm_simulate",  # households location choice model
        "jobs_relocation",  # jobs relocation model
        "jobs_transition",  # jobs transition
        "elcm_simulate",  # employment location choice
        "feasibility",  # compute development feasibility
        "residential_developer",  # develop residential buildings
        "non_residential_developer",  # develop non-residential buildings
    ],
    iter_vars=range(args.start, args.end),
    data_out=data_out,
    compress=True,
    out_run_local=False,
)


hdf = pd.HDFStore(data_out, mode="r")
csv_output_dir = "Output"

simulation_dir = (
    Path.cwd().joinpath(csv_output_dir).joinpath(f"result_{timestr}")
)
simulation_dir.mkdir(parents=True)

for filepath in hdf.keys():
    df = hdf.get(filepath)
    filename = filepath.replace("/", "") + ".csv"
    if "control" not in filename:
        file = simulation_dir.joinpath(filename)
        df.to_csv(file)
